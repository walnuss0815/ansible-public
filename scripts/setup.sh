#!/bin/bash

## Exit when any command fails
set -e

## Check if running with root permissions
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

## Check if curl is installed
if ! [ -x "$(command -v sudo)" ]; then
  echo "Install sudo"
  apt-get install sudo -y
fi

## Check if sudo is installed
if ! [ -x "$(command -v curl)" ]; then
  echo "Install curl"
  apt-get install curl -y
fi

## Check if python3 is installed
if ! [ -x "$(command -v python3)" ]; then
  echo "Install python3"
  apt-get install python3-pip -y
fi

## Set Ansible user credentials
ANSIBLE_USERNAME=ansible
ANSIBLE_PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)

## Add ansible user if not exists
if id -u $ANSIBLE_USERNAME >/dev/null 2>&1; then
    echo "Ansible user $ANSIBLE_USERNAME already exists"
else
    useradd --system --create-home --groups sudo $ANSIBLE_USERNAME
fi

## Change password
echo $ANSIBLE_USERNAME:$ANSIBLE_PASSWORD | chpasswd

## Print Ansible user credentials
echo
echo "Ansible user"
echo "Username: $ANSIBLE_USERNAME"
echo "Password: $ANSIBLE_PASSWORD"

## Disable sudo password for ansible user
echo "$ANSIBLE_USERNAME ALL=(ALL) NOPASSWD: ALL" > "/etc/sudoers.d/ansible"

## Print information
echo "---------------------------------------------------------------------------------"
echo "Now add the SSH key"
echo "Windows: ssh ansible@HOST -p 22 \"umask 644; test -d .ssh || mkdir .ssh ; cat >> .ssh/authorized_keys || exit 1\" < ansible\.ssh\id_rsa.pub"
echo "Linux: ssh-copy-id -i ansible/.ssh/id_rsa.pub ansible@HOST"

read -n1 -rsp $'Press any key to continue or Ctrl+C to exit...\n'

passwd -l $ANSIBLE_USERNAME
echo "Account $ANSIBLE_USERNAME is now locked for password login"